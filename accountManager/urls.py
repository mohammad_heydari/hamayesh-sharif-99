from django.urls import path
from . import views
urlpatterns = [
    path('register/', views.register_new_user, name='register-user'),
    path('all/', views.all_accounts, name='all-registered-accounts')
]