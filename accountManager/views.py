import json
import threading

from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from accountManager.forms import CreateUserForm, AccountForm
from accountManager.models import Account


def register_new_user(request):
    if not request.POST:
        return redirect('home-page')

    if User.objects.filter(username=request.POST['username']).exists():
        message = {
            'title': 'خطا!',
            'type': 'error',
            'description': 'این کد ملی قبلا ثبت شده است.'
        }
    else:
        user_form = CreateUserForm(request.POST)
        account_form = AccountForm(request.POST)
        if user_form.is_valid() and account_form.is_valid():
            user = user_form.save()

            account = account_form.save(commit=False)
            account.user = user
            account.save()

            message = {
                'title': 'موفقیت آمیز!',
                'type': 'success',
                'description': 'شما با موفقیت ثبت نام شدید.'
            }

            threading.Thread(target=account.send_success_email).start()
            
        else:
            message = {
                'title': 'خطا!',
                'type': 'error',
                'description': 'خطایی وجود دارد.'
            }
    data = {
        'message': message
    }

    return render(request, 'LastTemplate/HamayeshTemplate/students.sharif.ir/index.html', data)


@user_passes_test(lambda u: u.is_superuser)
@login_required
def all_accounts(request):
    accounts = []

    for account in Account.objects.all():
        accounts.append(
            [account.user.username, account.user.first_name, account.user.last_name, account.user.email,
             account.phone_number,
             account.rank, account.city, account.day])
    data = {
        'accounts': json.dumps(accounts)
    }
    return render(request, 'result.html', data)
