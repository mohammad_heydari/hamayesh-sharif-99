from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect


# Create your views here.
def login_request(request):
    if request.user.is_authenticated:
        return redirect('all-registered-accounts')
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user: User = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('all-registered-accounts')
    return render(request, 'login.html')
