from django.urls import path
from . import views
from django.contrib import admin

urlpatterns = [
    path('login/', views.login_request, name='login'),
    path('super/', admin.site.urls),
]
